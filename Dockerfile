#Imagen base
FROM node:latest

#Directorio de la app en el contenedor
WORKDIR /app

#Copiado de archivos
ADD build/default /app/build/default
ADD server.js /app
ADD package.json /app

#Dependencias
RUN npm install

#Puesto que expongo
EXPOSE 3000

#Comando NODEMON ES PARA PROBAR EN LOCAL, CUANDO SE SUBE A MÁQUINA, ES SOLO NODE
CMD ["npm","start"]
